Chief Shisha is an established company thriving throughout Sydney, delivering & hiring Shisha Statewide.

We are built upon foundations of excellence and professionalism, replicating high quality Shisha as well as genuine customer service with every order.

With aspirations of continued growth, we are set to meet our customers at their doorsteps as we continue to deliver the most superior quality Shisha to Sydney homes.

Website : https://www.chiefshisha.com.au/